#!/bin/bash

set -euf -o pipefail

run_container(){
    local vol_path=""
    local newuser=dev
    local run_args="-it --rm -w /mnt"
    while test $# -gt 0; do
        case "$1" in
            --volume-path)
                shift
                local vol_path=$1
                shift
                ;;
            *)
                run_args="$run_args $1"
                shift
                ;;
        esac
    done

    local usercommand
    usercommand="\"useradd -m $newuser && usermod -u $(id -u) $newuser 1>/dev/null && usermod -g $(id -g) $newuser 1>/dev/null && su $newuser\""
    if [ ! -z "$vol_path" ]; then
        run_args="$run_args --mount type=bind,source=$vol_path,target=/mnt"
    fi
    eval "docker run $run_args knipegp/riscv-tools:0.0.2 /bin/bash -c $usercommand"
}

run_container "$@"
