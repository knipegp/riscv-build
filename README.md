# RISC-V Tools

A Docker container including the
[riscv-gnu-toolchain](https://github.com/riscv/riscv-gnu-toolchain) compiled
for 32-bit and 64-bit Newlib and Linux libraries. The container is pushed to
[Docker Hub](https://hub.docker.com/repository/docker/knipegp/riscv-tools).

## Usage

Use the included `run_container.sh` script to run the container. The script
builds arguments to add a non-privileged user to the container and assigns the
host user's UID and GID to the container user. This enables the container user
to produce files that are owned by the host user.

Use `--volume-path <host path>` to attach a local directory at `/mnt` in the
container.

**Issue**

The container will not start in a bash environment. If bash is desired, run
`/bin/bash` in the container.
